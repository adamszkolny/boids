# Boids

Demo of [boids](https://en.wikipedia.org/wiki/Boids) simulation - form of artificial life acting like a flock of birds. Control agent's behaviour changing its components' weights with sliders.

- **Neighbour distance:** agents in this range are considered as neighbours 
- **Cohesion:** how much agent wants to stay next to its neighbours
- **Alignment:** how much agent wants to match its neighbours alignment
- **Separation:** how much agents wants to avoid its neighbours
- **Avoidance:** how much agents wants to avoid obstacles in its way

![Simulation demo screenshot](./Demo/img.png)

Project uses Unity 2020.3.31f1.

Build of the demo for Windows is in folder Demo/Windows.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Helpers
{
    public class TrigonometryHelper
    {
        /// <summary>
        /// Given line defined by points A and B returns point on this line closest to given point C.
        /// </summary>
        /// <param name="A">First point of line</param>
        /// <param name="B">Second point of line</param>
        /// <param name="C">Point</param>
        /// <returns>Point on line AB</returns>
        public static Vector3 GetClosestPointOnLine(Vector3 A, Vector3 B, Vector3 C)
        {
            Vector3 d = (A - B) / Vector3.Distance(A, B);
            Vector3 v = C - B;
            float t = Vector3.Dot(v, d);
            Vector3 P = B + new Vector3(t * d.x, t * d.y, t * d.z);
            return P;
        }

        /// <summary>
        /// Given line defined by points A and B returns point on this line closest to given point C.
        /// </summary>
        /// <param name="A">First point of line</param>
        /// <param name="B">Second point of line</param>
        /// <param name="C">Point</param>
        /// <returns>Point on line AB</returns>
        public static Vector2 GetClosestPointOnLine(Vector2 A, Vector2 B, Vector2 C)
        {
            Vector2 d = (A - B) / Vector3.Distance(A, B);
            Vector2 v = C - B;
            float t = Vector2.Dot(v, d);
            Vector2 P = B + new Vector2(t * d.x, t * d.y);
            return P;
        }
    }

}
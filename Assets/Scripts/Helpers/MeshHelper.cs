using UnityEngine;

namespace Helpers
{
    public class MeshHelper
    {
        /// <returns>Array of 3 Vector3s being triangle's vertices</returns>
        public static Vector3[] GetVerticesOfTriangle(Mesh mesh, int triangleIndex)
        {
            return new Vector3[]
            {
                mesh.vertices[mesh.triangles[triangleIndex * 3]],
                mesh.vertices[mesh.triangles[triangleIndex * 3 + 1]],
                mesh.vertices[mesh.triangles[triangleIndex * 3 + 2]],
            };
        }

        /// <returns>Array of 3 Vector3s being triangle's normals</returns>
        public static Vector3[] GetNormalsOfTriangle(Mesh mesh, int triangleIndex)
        {
            return new Vector3[]
            {
                mesh.normals[mesh.triangles[triangleIndex * 3]],
                mesh.normals[mesh.triangles[triangleIndex * 3 + 1]],
                mesh.normals[mesh.triangles[triangleIndex * 3 + 2]],
            };
        }
    }

}
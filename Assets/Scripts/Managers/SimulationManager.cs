using Boids;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Boids
{
    public class SimulationManager : MonoBehaviour
    {
        [SerializeField]
        private List<GameObject> _playgrounds;
        [SerializeField]
        private Flock _flock;

        [Header("Labels")]
        [SerializeField]
        private TextMeshProUGUI _agentsCountLabel;
        [SerializeField]
        private TextMeshProUGUI _speedLabel;
        [SerializeField]
        private TextMeshProUGUI _neighbourDistLabel;
        [Space]
        [SerializeField]
        private CompositeBehaviour _behaviours;

        public bool IsPlaying => _flock.EnableMovement;

        public void PlayPause()
        {
            _flock.EnableMovement = !_flock.EnableMovement;
        }

        public void RestartSimulation()
        {
            _flock.Initialize();
        }

        public void SetSpeed(float speed)
        {
            _flock.Speed = speed;
            _speedLabel.text = $"Speed {speed:n1}";
        }

        public void SetActivePlayground(int playgroundIndex)
        {
            if (playgroundIndex < 0 || playgroundIndex > _playgrounds.Count - 1)
            {
                return;
            }

            for (int i = 0; i < _playgrounds.Count; i++)
            {
                _playgrounds[i].gameObject.SetActive(i == playgroundIndex);
            }
        }

        public void SetNeighbourDistance(float distance)
        {
            _flock.NeighbourDistance = distance;
            _neighbourDistLabel.text = $"Neighbour\ndistance: {distance:n1}";
        }

        public void SetAgentsCount(float count)
        {
            SetAgentsCount((int)count);
        }

        public void SetAgentsCount(int count)
        {
            _flock.AgentsCount = count;
            _agentsCountLabel.text = $"Agents: {count}";
        }

        public void SetWeight_1(float value)
        {
            _behaviours.Weights[0] = value;
        }

        public void SetWeight_2(float value)
        {
            _behaviours.Weights[1] = value;
        }

        public void SetWeight_3(float value)
        {
            _behaviours.Weights[2] = value;
        }

        public void SetWeight_4(float value)
        {
            _behaviours.Weights[3] = value;
        }
    }

}
using System.Collections.Generic;
using UnityEngine;

namespace Boids
{
    [CreateAssetMenu(fileName = "SameFlock", menuName = "Flock Filters/Same Flock")]
    public class SameFlockFilter : ContextFilter
    {
        public override List<Transform> Filter(FlockAgent agent, List<Transform> original)
        {
            List<Transform> filtered = new List<Transform>();
            foreach (var item in original)
            {
                var otherAgent = item.GetComponent<FlockAgent>();
                if (otherAgent == null)
                {
                    continue;
                }
                if (agent.Flock == otherAgent.Flock)
                {
                    filtered.Add(item);
                }
            }

            return filtered;
        }
    }

}
using System.Collections.Generic;
using UnityEngine;

namespace Boids
{
    [CreateAssetMenu(fileName = "PhysicsLayer", menuName = "Flock Filters/Physics Layer")]
    public class PhysicsLayerFilter : ContextFilter
    {
        [SerializeField]
        private LayerMask _layerMask;

        public override List<Transform> Filter(FlockAgent agent, List<Transform> original)
        {
            List<Transform> filtered = new List<Transform>();
            foreach (var item in original)
            {
                var otherAgent = item.GetComponent<FlockAgent>();
                if (otherAgent == null)
                {
                    continue;
                }
                if (otherAgent.gameObject.layer != _layerMask)
                {
                    Debug.Log("CHUJ");
                    filtered.Add(item);
                }
            }

            return filtered;
        }
    }

}
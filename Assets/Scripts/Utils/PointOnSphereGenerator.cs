using System.Collections.Generic;
using UnityEngine;

public class PointOnSphereGenerator : MonoBehaviour
{

    private List<Vector3> _points = new List<Vector3>();

    void Start()
    {
        _points = new List<Vector3>(GenerateFibonacciSphere(100));
    }

    private void OnDrawGizmos()
    {
        foreach (var point in _points)
        {
            Gizmos.DrawWireCube(point, Vector3.one * 0.1f);
        }
    }

    public static Vector3 GetRandomPointOnSphere(float radius, Vector3 center)
    {
        float theta = Random.Range(0, Mathf.PI);
        float phi = Random.Range(0, 2 * Mathf.PI);
        return new Vector3(
            Mathf.Sin(theta) * Mathf.Cos(phi) * radius,
            Mathf.Sin(theta) * Mathf.Sin(phi) * radius,
            Mathf.Cos(theta) * radius) + center;
    }

    public static Vector3[] GenerateFibonacciSphere(int samples, float sphereRadius = 1f)
    {
        Vector3[] points = new Vector3[samples];
        float phi = Mathf.PI * (3f - Mathf.Sqrt(5f)); // Golder angle in radians

        for (int i = 0; i < points.Length; i++)
        {
            float y = 1f - (i / (float)(samples - 1)) * 2f; // Y goes from -1 to 1
            float radius = Mathf.Sqrt(1f - y * y); // Radius at y
            float theta = phi * i;
            float x = Mathf.Cos(theta) * radius;
            float z = Mathf.Sin(theta) * radius;
            points[i] = new Vector3(x * sphereRadius, y * sphereRadius, z * sphereRadius);
        }

        return points;
    }

    /// <summary>
    /// theta - polar angle [0; PI]
    /// phi = azimuthal angle [0; 2PI]
    /// </summary>
    /// <param name="samples"></param>
    /// <param name="sphereRadius"></param>
    /// <returns></returns>
    public static Vector3[] GenerateRegularDistributedPoints(int samples, float sphereRadius)
    {
        Vector3[] points = new Vector3[samples];
        int i = 0;
        float a = (4 * Mathf.PI * sphereRadius * sphereRadius) / samples;
        float d = Mathf.Sqrt(a);            // d - distance on sphere between points
        //float M_theta = Mathf.FloorToInt(Mathf.PI / d);
        float M_theta = Mathf.CeilToInt((Mathf.PI*sphereRadius) / d);   // M_theta - count of points in polar direction
        float d_theta = Mathf.PI / M_theta;                             // d_theta - angle delta in polar direction
        float d_phi = a / d_theta;                                      // it makes no sense!!!
        //for (int m = 0; m < M_theta; m++)    // latitude
        for (int m = 0; m < M_theta; m++)    // latitude
        {
            float theta = (Mathf.PI * (m + 0.5f)) / M_theta;
            //float M_phi = Mathf.CeilToInt(2 * Mathf.PI * Mathf.Sin(theta / d_phi));
            float M_phi = Mathf.CeilToInt(((Mathf.PI * sphereRadius) / d) * Mathf.Cos(theta));
            for (int n = 0; n < M_phi; n++)    // longitude
            {
                float phi = 2 * Mathf.PI * n / M_phi;
                points[i] = new Vector3(Mathf.Sin(theta) * Mathf.Cos(phi) * sphereRadius,
                    Mathf.Sin(theta) * Mathf.Sin(phi) * sphereRadius,
                    Mathf.Cos(theta));
                i += 1;
            }
        }
        return points;
    }

    private static Vector3 GenerateRandomPointsOnSphere(Vector3 sphereCenter, float radius)
    {
        var point = Random.insideUnitSphere;
        point.Normalize();
        point += sphereCenter;
        point *= radius;
        return point;
    }
}

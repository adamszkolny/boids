using Helpers;
using System.Collections.Generic;using System.Linq;
using UnityEngine;namespace Boids{    [RequireComponent(typeof(Collider))]    public class FlockAgent : MonoBehaviour    {        private Collider _collider;        private Flock _flock;        private float _detectionDistance = 0f;        private Color _gizmoRaycastColor = Color.white;        private Vector3 _edgePoint = Vector3.zero;        private Vector3 _beyondEdgePoint = Vector3.zero;        private int _gizmoLinesCount = 0;        private Vector3 _unobstructedDirection = Vector3.zero;        private RaycastGizmo[] _rayGizmos = new RaycastGizmo[20];        public Collider Collider => _collider;        public Flock Flock => _flock;        private void Start()        {            _collider = GetComponent<Collider>();        }        public void Initialize(Flock flock)        {            _flock = flock;            var points = PointOnSphereGenerator.GenerateFibonacciSphere(_rayGizmos.Length, 10f);
            for (int i = 0; i < points.Length; i++)
            {
                _rayGizmos[i] = new RaycastGizmo(points[i], Color.white);
            }

            // sort rays by angle
            _rayGizmos = _rayGizmos.OrderBy(r =>                 Vector3.Angle(transform.forward, transform.TransformDirection(r.direction)))                .ToArray();        }        public void Move(Vector3 movement)        {            if (movement == Vector3.zero)
            {
                return;
            }

            transform.forward = movement;            transform.position += movement * Time.deltaTime;        }        private void OnDrawGizmos()
        {
            //foreach (var ray in _rayGizmos)
            for (int i = 0; i < _gizmoLinesCount; i++)
            {
                var ray = _rayGizmos[i];
                Gizmos.color = ray.color;
                Vector3 rayDirection = new Vector3(ray.direction.x * _detectionDistance,
                                 ray.direction.y * _detectionDistance,
                                 ray.direction.z * _detectionDistance);
                Gizmos.DrawLine(transform.position, transform.position + transform.TransformDirection(rayDirection));
            }

            Gizmos.color = Color.green;
            Gizmos.DrawLine(transform.position, transform.position + transform.forward);
        }        public bool FindUnobstructedDirection(float detectionAngle, LayerMask mask, float detectionDistance, out Vector3 unobstructedDirection)
        {
            RaycastHit hitInfo;
            _detectionDistance = detectionDistance;
            unobstructedDirection = Vector3.zero;

            _gizmoLinesCount = 0;
            if (!Physics.SphereCast(transform.position, 0.5f, transform.forward, out hitInfo, detectionDistance, mask))
            {
                return false;
            }

            for (int i = 0; i < _rayGizmos.Length; i++)
            {
                _gizmoLinesCount = i + 1;
                var transformedDirection = transform.TransformDirection(_rayGizmos[i].direction);   

                // Filtering rays with too high angle
                if (Vector3.Angle(transform.forward, transformedDirection) > detectionAngle)
                {
                    _rayGizmos[i].color = Color.black;
                    continue;
                }

                //if (Physics.Raycast(transform.position, transformedDirection.normalized, out hitInfo, detectionDistance, mask))
                if (Physics.SphereCast(transform.position, 0.5f, transformedDirection.normalized, out hitInfo, detectionDistance, mask))
                {
                    _rayGizmos[i].color = Color.red;
                }
                else
                {
                    unobstructedDirection = transformedDirection.normalized;
                    _rayGizmos[i].color = Color.white;
                    return true;
                }
            }

            return false;        }        /// <summary>
        /// DO NOT USE
        /// The method should return point on silhouette's edge closest to raycast hit. It works properly only in some cases.
        /// Leaving it here right now, might revisit in the future.
        /// </summary>        [System.Obsolete("Doesn't work for all cases. Do not use.", false)]        public bool GetPointOnSilhouette(Vector3 direction, LayerMask mask, float detectionDistance, ref Vector3 pointOnEdge)        {            RaycastHit hitInfo;            _detectionDistance = detectionDistance;            _gizmoRaycastColor = Color.white;            _edgePoint = Vector3.zero;            if (Physics.Raycast(transform.position, direction, out hitInfo, detectionDistance, mask))            {                var hitObject = hitInfo.transform;                var point = hitInfo.point;                var normal = hitInfo.normal;                var gameObject = hitInfo.collider.gameObject;                var mesh = gameObject.GetComponent<MeshFilter>().mesh;                int triangleIndex = hitInfo.triangleIndex;                var hitFace = new MeshFace(mesh, triangleIndex);                //var face = GetFace(mesh, triangleIndex, hitObject);
                _gizmoRaycastColor = Color.red;

                Debug.Log(point);                var oppositeFacesIndices = new List<int>();                                // For each face                for (int i = 0; i < mesh.triangles.Length/3; i++)                {                    var face = new MeshFace(mesh, i);                    var dotVector = Vector3.Dot(normal, hitObject.localToWorldMatrix.MultiplyVector(face.Normal));
                    //if (Vector3.Dot(normal, hitObject.localToWorldMatrix.MultiplyPoint3x4(face.Normal)) < 0)
                    //if (Vector3.Dot(normal, hitObject.localToWorldMatrix.MultiplyVector(face.Normal)) < 0)
                    //if (Vector3.Dot(normal, hitObject.TransformDirection(face.Normal)) < 0)
                    var toSourceVector = transform.position - point;                    if (Vector3.Dot(toSourceVector, hitObject.TransformDirection(face.Normal)) < 0)                    {                        oppositeFacesIndices.Add(i);
                        Debug.Log($"Normals {i} {toSourceVector} {hitObject.localToWorldMatrix.MultiplyVector(face.Normal)}\n" +
                            $"Dot {dotVector}");                    }                }                float minDistanceToEdge = float.MaxValue;                Vector3 closestPointOnClosestEdge = Vector3.zero;                // Get closest silhouette edge to hit point                foreach (int index in oppositeFacesIndices)                {                    var oppositeFace = new MeshFace(mesh, index);                    var p1 = TrigonometryHelper.GetClosestPointOnLine(                        hitObject.localToWorldMatrix.MultiplyPoint3x4(oppositeFace.vertices[1]),                        hitObject.localToWorldMatrix.MultiplyPoint3x4(oppositeFace.vertices[2]),                        point);                    if (Vector3.Distance(p1, point) < minDistanceToEdge)
                    {
                        closestPointOnClosestEdge = p1;
                        minDistanceToEdge = Vector3.Distance(p1, point);
                    }
                    var p2 = TrigonometryHelper.GetClosestPointOnLine(
                        hitObject.localToWorldMatrix.MultiplyPoint3x4(oppositeFace.vertices[1]),                        hitObject.localToWorldMatrix.MultiplyPoint3x4(oppositeFace.vertices[2]),
                        point);
                    if (Vector3.Distance(p2, point) < minDistanceToEdge)
                    {
                        closestPointOnClosestEdge = p2;
                        minDistanceToEdge = Vector3.Distance(p2, point);
                    }                    var p3 = TrigonometryHelper.GetClosestPointOnLine(
                        hitObject.localToWorldMatrix.MultiplyPoint3x4(oppositeFace.vertices[2]),                        hitObject.localToWorldMatrix.MultiplyPoint3x4(oppositeFace.vertices[0]),
                        point);
                    if (Vector3.Distance(p3, point) < minDistanceToEdge)
                    {
                        closestPointOnClosestEdge = p3;
                        minDistanceToEdge = Vector3.Distance(p3, point);
                    }                }                pointOnEdge = closestPointOnClosestEdge;                _edgePoint = closestPointOnClosestEdge;                Vector2 projectedPosition = Vector2.zero;                var toEdgeVector = pointOnEdge - point;
                var agentLength = 2f;
                _beyondEdgePoint = _edgePoint + Vector3.Scale(Vector3.one * agentLength, Vector3.ProjectOnPlane(toEdgeVector, -transform.forward).normalized);                pointOnEdge = _beyondEdgePoint;                return true;            }            return false;        }

        private struct RaycastGizmo
        {
            public Vector3 direction;
            public Color color;

            public RaycastGizmo(Vector3 direction, Color color)
            {
                this.direction = direction.normalized;
                this.color = color;
            }
        }    }    public struct MeshFace
    {
        public Vector3[] vertices;        public Vector3[] normals;

        public Vector3 Normal => (normals[0] + normals[1] + normals[2]) / 3;        public MeshFace(Mesh mesh, int triangleIndex)
        {
            vertices = new Vector3[]
            {
                mesh.vertices[mesh.triangles[triangleIndex * 3]],
                mesh.vertices[mesh.triangles[triangleIndex * 3 + 1]],
                mesh.vertices[mesh.triangles[triangleIndex * 3 + 2]],
            };            normals = new Vector3[]
            {                mesh.normals[mesh.triangles[triangleIndex * 3]],
                mesh.normals[mesh.triangles[triangleIndex * 3 + 1]],
                mesh.normals[mesh.triangles[triangleIndex * 3 + 2]],            };
        }    }}
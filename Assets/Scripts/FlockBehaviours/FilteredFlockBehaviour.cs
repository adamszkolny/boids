using UnityEngine;

namespace Boids
{
    public abstract class FilteredFlockBehaviour : FlockBehaviour
    {
        [SerializeField]
        protected ContextFilter _filter;
    }

}
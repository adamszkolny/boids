using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Boids
{
    [CreateAssetMenu(fileName = "SteerToAvoid_Raycast", menuName = "Flock Behaviours/Steer To Avoid")]
    public class SteerToAvoidBehaviour : FilteredFlockBehaviour
    {
        [SerializeField]
        private LayerMask _mask;
        [SerializeField]
        [Range(0f, 10f)]
        private float _detectionDistance = 2f;

        public override Vector3 CalculateMovement(FlockAgent agent, List<Transform> context, Flock flock)
        {
            if (context.Count == 0)
            {
                return Vector3.zero;
            }

            Vector3 avoidanceMovement = Vector3.zero;
            Vector3 unobstructedDirection = Vector3.one;

            if (agent.FindUnobstructedDirection(120f, _mask, _detectionDistance, out unobstructedDirection))
            {
                avoidanceMovement = unobstructedDirection;
            }

            return avoidanceMovement;
        }
    }

}
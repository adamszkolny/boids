using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Boids
{
    [CreateAssetMenu(fileName = "CompositeBehaviour", menuName = "Flock Behaviours/Conmposite")]
    public class CompositeBehaviour : FlockBehaviour
    {
        [SerializeField]
        private FlockBehaviour[] _behaviours;
        [SerializeField]
        private float[] _weights;

        public float[] Weights { get => _weights; set => _weights = value; }


        public override Vector3 CalculateMovement(FlockAgent agent, List<Transform> context, Flock flock)
        {
            if (_weights.Length != _behaviours.Length)
            {
                throw new System.IndexOutOfRangeException();
            }

            Vector3 movement = Vector3.zero;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("Behaviours:");
            for (int i = 0; i < _behaviours.Length; i++)
            {
                var partialMove = _behaviours[i].CalculateMovement(agent, context, flock) * _weights[i];
                stringBuilder.AppendLine(_behaviours[i].name + partialMove.ToString());
                if (partialMove != Vector3.zero)
                {
                    if (partialMove.sqrMagnitude > _weights[i] * _weights[i])
                    {
                        partialMove.Normalize();
                        partialMove *= _weights[i];
                    }

                    movement += partialMove;
                }
                //Debug.Log($"Partial move = {partialMove}");
            }

            //Debug.Log(stringBuilder.ToString());

            return movement;
        }
    }

}
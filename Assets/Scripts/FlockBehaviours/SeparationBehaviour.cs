using System.Collections.Generic;
using UnityEngine;

namespace Boids
{
    [CreateAssetMenu(fileName = "Separation", menuName = "Flock Behaviours/Separation")]
    public class SeparationBehaviour : FilteredFlockBehaviour
    {
        public override Vector3 CalculateMovement(FlockAgent agent, List<Transform> context, Flock flock)
        {
            if (context.Count == 0)
            {
                return Vector3.zero;
            }

            // Add all points together and average
            Vector3 avoidanceMovement = Vector2.zero;
            int avoidCount = 0;
            List<Transform> filteredContext = (_filter == null) ? context : _filter.Filter(agent, context);
            foreach (Transform item in filteredContext)
            {
                if (Vector3.SqrMagnitude(item.position - agent.transform.position) < flock.SquareAvoidanceRadius)
                {
                    avoidanceMovement += agent.transform.position - item.transform.position;
                    avoidCount += 1;
                }
            }

            if (avoidCount > 0)
            {
                avoidanceMovement /= avoidCount;
            }

            return avoidanceMovement;
        }
    }

}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Boids
{
    [CreateAssetMenu(fileName = "AlignmentBehaviour", menuName = "Flock Behaviours/Alignment")]

    public class AlignmentBehaviour : FilteredFlockBehaviour
    {
        public override Vector3 CalculateMovement(FlockAgent agent, List<Transform> context, Flock flock)
        {
            if(context.Count == 0)
            {
                return agent.transform.forward;
            }

            // Add all points together and average
            Vector3 alignemntMovement = Vector2.zero;
            List<Transform> filteredContext = (_filter == null) ? context : _filter.Filter(agent, context);
            foreach (Transform item in filteredContext)
            {
                alignemntMovement += item.transform.forward;
            }
            alignemntMovement /= context.Count;

            return alignemntMovement;
        }
    }

}
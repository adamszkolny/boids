using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Boids
{
    [CreateAssetMenu(fileName = "SeekGoal", menuName = "Flock Behaviours/Seek Goal")]
    public class SeekGoal : FilteredFlockBehaviour
    {
        [SerializeField]
        [Range(0,2f)]
        private float _minSeekDistance = 1f;

        public override Vector3 CalculateMovement(FlockAgent agent, List<Transform> context, Flock flock)
        {
            var toGoalVector = flock.FlockGoal.position - agent.transform.position;

            if (toGoalVector.magnitude < _minSeekDistance)
            {
                return Vector2.zero;
            }

            return toGoalVector.normalized;
        }
    }

}
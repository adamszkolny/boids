using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Boids
{
    [CreateAssetMenu(fileName = "SteeredCohesion", menuName = "Flock Behaviours/Steered Cohesion")]
    public class SteeredCohesionBehaviour : FilteredFlockBehaviour
    {
        [SerializeField]
        private float _agentSmoothTime = 0.5f;
        private Vector3 _currentVelocity;

        public override Vector3 CalculateMovement(FlockAgent agent, List<Transform> context, Flock flock)
        {
            if (context.Count == 0)
            {
                return Vector3.zero;
            }

            // Add all points together and average
            Vector3 cohesionMovement = Vector2.zero;
            List<Transform> filteredContext = (_filter == null) ? context : _filter.Filter(agent, context);
            foreach (Transform item in filteredContext)
            {
                cohesionMovement += item.position;
            }
            cohesionMovement /= context.Count;

            // Create offset from agent position
            cohesionMovement -= agent.transform.position;
            cohesionMovement = Vector3.SmoothDamp(agent.transform.forward, cohesionMovement, ref _currentVelocity, _agentSmoothTime);
            return cohesionMovement;
        }


    }

}
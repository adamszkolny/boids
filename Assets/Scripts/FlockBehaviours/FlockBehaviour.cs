using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Boids
{
    public abstract class FlockBehaviour : ScriptableObject
    {
        public abstract Vector3 CalculateMovement(FlockAgent agent, List<Transform> context, Flock flock);
    }

}
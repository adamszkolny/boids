using System.Collections.Generic;
using UnityEngine;

namespace Boids
{
    public class Flock : MonoBehaviour
    {
        private const float AgentDensity = 0.1f;

        [SerializeField]
        private FlockAgent _flockAgentPrefab;
        [SerializeField]
        private FlockBehaviour _flockBehaviour;
        [SerializeField]
        [Range(1f, 20f)]
        private float _spawnRadius = 5f;
        [SerializeField]
        [Range(1, 200)]
        private int _agentsCount = 50;
        [SerializeField]
        [Range(1f, 20f)]
        private float _driveFactor = 10f;
        [SerializeField]
        [Range(1f, 10f)]
        private float _neighbourDistance = 1.5f;
        [SerializeField]
        [Range(0f, 1f)]
        private float _avoidanceRadiusMultiplier = 0.5f;
        [SerializeField]
        private Transform _flockCenter;
        [SerializeField]
        private Transform _flockGoal;
        [SerializeField]
        private bool _enableMovement = true;

        private List<FlockAgent> _agents = new List<FlockAgent>();
        private float _squareMaxSpeed;
        private float _squareNeighbourDistance;
        private float _squareAvoidanceRadius;

        public float SquareAvoidanceRadius => _squareAvoidanceRadius;
        public Transform FlockCenter => _flockCenter;
        public Transform FlockGoal => _flockGoal;
        public bool EnableMovement { get => _enableMovement; set => _enableMovement = value; }
        public int AgentsCount { get => _agentsCount; set => _agentsCount = value; }
        public float Speed { get => _driveFactor; set => _driveFactor = Mathf.Clamp(value, 1f, 20f); }
        public float NeighbourDistance { 
            get => _neighbourDistance;
            set 
            { 
                _neighbourDistance = Mathf.Clamp(value, 1f, 10f);
                CalculateSquares();
            }
        }

        int _tickCount;

        void Start()
        {
            CalculateSquares();
            Initialize();
        }

        public void Initialize()
        {
            DestroyAllAgents();
            SpawnAgents();
        }

        private void DestroyAllAgents()
        {
            foreach (var agent in _agents)
            {
                GameObject.Destroy(agent.gameObject);
            }
            _agents.Clear();
        }

        private void SpawnAgents()
        {
            for (int i = 0; i < _agentsCount; i++)
            {
                Vector3 randomPoint = PointOnSphereGenerator.GetRandomPointOnSphere(_spawnRadius, _flockCenter.position);
                Quaternion rotation = Quaternion.LookRotation((randomPoint - _flockCenter.position), Vector3.up);
                FlockAgent newAgent = Instantiate(
                    _flockAgentPrefab,
                    //Random.insideUnitSphere * _startingCount * AgentDensity + _flockCenter.position,
                    randomPoint,
                    //Random.rotation,
                    rotation,
                    transform);
                newAgent.name = $"Agent {i.ToString("D3")}";
                newAgent.Initialize(this);
                _agents.Add(newAgent);
            }
        }

        void FixedUpdate()
        {
            Tick();
        }

        private void Tick()
        {
            foreach (FlockAgent agent in _agents)
            {
                List<Transform> context = GetNearbyObjects(agent);

                Vector3 movement = _flockBehaviour.CalculateMovement(agent, context, this);
                movement.Normalize();
                movement *= _driveFactor;
                if (_enableMovement)
                {
                    agent.Move(movement);
                }
            }
        }

        private List<Transform> GetNearbyObjects(FlockAgent agent)
        {
            List<Transform> context = new List<Transform>();
            Collider[] contextColliders = Physics.OverlapSphere(agent.transform.position, _neighbourDistance);
            foreach (Collider c in contextColliders)
            {
                if (c != agent.Collider)
                {
                    context.Add(c.transform);
                }
            }

            return context;
        }

        private void OnValidate()
        {
            CalculateSquares();
        }

        private void CalculateSquares()
        {
            //_squareMaxSpeed = -_maxSpeed * _maxSpeed;
            _squareNeighbourDistance = _neighbourDistance * _neighbourDistance;
            _squareAvoidanceRadius = _squareNeighbourDistance * _avoidanceRadiusMultiplier /** _avoidanceRadiusMultiplier*/;
        }
    }

}
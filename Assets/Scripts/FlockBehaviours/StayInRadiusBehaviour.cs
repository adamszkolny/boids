using System.Collections.Generic;
using UnityEngine;

namespace Boids
{
    [CreateAssetMenu(fileName = "StayInRadiusBehaviour", menuName = "Flock Behaviours/Stay In Radius")]
    public class StayInRadiusBehaviour : FlockBehaviour
    {
        [SerializeField]
        [Range(10, 200f)]
        private float _radius = 50f;

        public override Vector3 CalculateMovement(FlockAgent agent, List<Transform> context, Flock flock)
        {
            var centerOffset = flock.FlockCenter.position - agent.transform.position;
            float t = centerOffset.magnitude / _radius;

            if (t < 0.9f)
            {
                return Vector2.zero;
            }

            return centerOffset * t * t;
        }
    }

}